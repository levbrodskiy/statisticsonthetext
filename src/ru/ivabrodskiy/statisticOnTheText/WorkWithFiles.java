package ru.ivabrodskiy.statisticOnTheText;


import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * Класс реализует работу с файлами
 */
public class WorkWithFiles {


    /**
     * Метод реализует чтение из файла
     * @param way - путь к файлу
     * @return данные файла
     * @throws IOException
     */
    public static List<String> read(String way) throws IOException {
        List<String> list;
        try {
            list = (Files.readAllLines(Paths.get(way), StandardCharsets.UTF_8));
        } catch (IOException e) {
            throw new IOException();
        }
        return list;
    }

    /**
     * Метод реализует запись данных в файл
     * @param way - путь к файлу
     * @param list - данные
     * @throws IOException
     */
    public static void write(String way, List<String> list) throws IOException {

        try (FileWriter fw = new FileWriter(way)) {
            for (String i : list) {
                fw.write(i);
                fw.write("\n");
            }
        } catch (Exception e) {
            throw new IOException();

        }
    }
}