package ru.ivabrodskiy.statisticOnTheText;

/**
 * Класс рассчёта статистики по тексту
 */
public class StatisticOnTheText {

    /**
     * Метод возвращает количество символов в тексте
     *
     * @param text - обрабатываемый текст
     * @return количество символов в тексте
     */
    public static long countAllSymbols(String text) {
        return text.length();
    }

    /**
     * Метод возвращает количество символов в тексте( без пробелов )
     *
     * @param text - обрабатываемый текст
     * @return количество символов в тексте( без пробелов )
     */
    public static long countSymbolsWithoutGaps(String text) {
        text = text.replaceAll("\\s", "");
        return text.length();
    }


    /**
     * Метод возвращает количество слов в тексте
     *
     * @param text - обрабатываемый текст
     * @return количество слов в тексте
     */
    public static long countAllWords(String text) {
        int numberOfWords = 0;
        String textReplace = text.replaceAll("[\"\\\\\\[\\]\\/\\*\\+\\.\\,\\(\\)\\&\\?\\%\\$\\#\\@\\!\\;\\№]+"," ");
        String[] splitLine = textReplace.split("\\s");
        for (int i = 0; i < splitLine.length; i++) {
            if(splitLine[i].isEmpty()||splitLine[i] == ""||splitLine[i].matches("[-]+")){
                continue;
            }
           
            numberOfWords++;
        }
        return numberOfWords;
    }
}