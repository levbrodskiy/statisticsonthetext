package ru.ivabrodskiy.statisticOnTheText;

import java.util.List;

/**
 * Класс реализует работу с консолью
 */
public class WorkWithConsol {

    /**
     * Метод выводит в консоль данные
     *
     * @param list - данные для вывода
     */
    public static void print(List<String> list) {
        for (String i:list) {
            System.out.println(i);
        }
    }
}