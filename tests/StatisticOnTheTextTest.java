import ru.ivabrodskiy.statisticOnTheText.StatisticOnTheText;

import static org.junit.Assert.*;

public class StatisticOnTheTextTest {

    @org.junit.Test
    public void AllSEmpty() {
        String expected = "";
        long actual = 0;
        assertEquals(StatisticOnTheText.countAllSymbols(expected),actual);
    }

    @org.junit.Test
    public void AllSOnlyGaps() {
        String expected = "   ";
        long actual = 3;
        assertEquals(StatisticOnTheText.countAllSymbols(expected),actual);
    }

    @org.junit.Test
    public void AllSWithEnglishLetters() {
        String expected = "error";
        long actual = 5;
        assertEquals(StatisticOnTheText.countAllSymbols(expected),actual);
    }


    @org.junit.Test
    public void SWGOnlyGaps() {
        String expected = "     ";
        long actual = 0;
        assertEquals(StatisticOnTheText.countSymbolsWithoutGaps(expected),actual);
    }

    @org.junit.Test
    public void SWGapsEmpty() {
        String expected = "";
        long actual = 0;
        assertEquals(StatisticOnTheText.countSymbolsWithoutGaps(expected),actual);
    }

    @org.junit.Test
    public void SWGapsEnglishLetters() {
        String expected = "rer";
        long actual = 3;
        assertEquals(StatisticOnTheText.countSymbolsWithoutGaps(expected),actual);
    }

    @org.junit.Test
    public void SWGEnglishLettersAndGaps() {
        String expected = "r r";
        long actual = 2;
        assertEquals(StatisticOnTheText.countSymbolsWithoutGaps(expected),actual);
    }

    @org.junit.Test
    public void AWEmpty() {
        String expected = "";
        long actual = 0;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }

    @org.junit.Test
    public void AWOnlyGaps() {
        String expected = "     ";
        long actual = 0;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }

    @org.junit.Test
    public void AWEnglishLetters() {
        String expected = "rerer";
        long actual = 1;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }

    @org.junit.Test
    public void AWFullText() {
        String expected = "drfgh 5/  */  5/ -5 5- - r-r енро шо п8 0п 9пагиль рим 89 8*";
        long actual = 14;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }

    @org.junit.Test
    public void AWSpecialSymbols() {
        String expected = "! /*";
        long actual = 0;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }

    @org.junit.Test
    public void AWSpecialSymbols2() {
        String expected = "-";
        long actual = 0;
        assertEquals(StatisticOnTheText.countAllWords(expected),actual);
    }
}